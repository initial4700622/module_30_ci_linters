from subprocess import run

if __name__ == "__main__":
    """
    Start application 'Cookbook'.
    """

    run("uvicorn routes:app --reload", shell=True, stdout=True, stderr=True)
