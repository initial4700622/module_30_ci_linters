from typing import Dict, Optional

from fast_api.database import Recipe


async def handle_all_recipies():
    result = await Recipe.get_all_recipes()
    all_recipies = list()
    for i_recipe in result:
        recipy = dict()
        recipy["name"] = i_recipe[0]
        recipy["views"] = i_recipe[1]
        recipy["cooking_time_in_min"] = i_recipe[2]
        all_recipies.append(recipy)

    return all_recipies


async def handle_recipe_details(recipy_idx: int) -> Optional[dict]:
    recipy = await Recipe.get_recipy_details(recipy_idx)
    if recipy:
        recipy_details: Dict = dict()
        recipy_details["ingredients"] = list()
        recipy_details["name"] = recipy.name
        recipy_details["cooking_time_in_min"] = recipy.cooking_time_in_min
        recipy_details["description"] = recipy.description
        for i_ingredient in recipy.ingredients:
            recipy_details["ingredients"].append(str(i_ingredient.name))
        recipy_details["ingredients"].sort()

        return recipy_details

    return None
