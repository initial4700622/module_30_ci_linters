from asyncio import run
from os import path, remove

import pytest
from httpx import ASGITransport, AsyncClient

from ..database import init_db
from ..routes import app

correct_recipe_1_message = {
    "name": "«Салат Цезарь»",
    "cooking_time_in_min": 60,
    "description": "Традиционный с курицей",
    "ingredients": [
        "Белый хлеб",
        "Зеленый салат",
        "Куриное филе",
        "Помидоры",
        "Соус «Цезарь»",
        "Сыр пармезан",
    ],
}
correct_recipe_0_error_message = {
    "error_massage": "There is no recipy with provided id"
}
correct_all_recipes_message = [
    {"name": "«Салат Весенний»", "views": 0, "cooking_time_in_min": 10},
    {"name": "«Салат Цезарь»", "views": 0, "cooking_time_in_min": 60},
    {"name": "«Салат Винегрет»", "views": 0, "cooking_time_in_min": 150},
]
pytest_plugins = ("pytest_asyncio",)


@pytest.fixture(scope="module", autouse=True)
def initialize_db():
    """Remove db and initialize again for testing purpose"""

    if path.exists("./cookbook.db"):
        remove("./cookbook.db")
    run(init_db())


class TestRoutes:
    """Tests endpoints /recipes and /recipe/{recipy_idx}"""

    @pytest.mark.asyncio
    async def test_all_recipes_status_code_200(self) -> None:
        """Test that endpoint /recipes return http status code 200"""

        async with AsyncClient(
            transport=ASGITransport(app=app), base_url="http://test"
        ) as ac:
            response = await ac.get("/recipes")
            assert response.status_code == 200

    @pytest.mark.asyncio
    async def test_all_recipes_success_response_message(self) -> None:
        """Test that endpoint /recipes return correct response data"""

        async with AsyncClient(
            transport=ASGITransport(app=app), base_url="http://test"
        ) as ac:
            response = await ac.get("/recipes")
            assert response.json() == correct_all_recipes_message

    @pytest.mark.asyncio
    async def test_recipe_details_status_code_200(self) -> None:
        """Test that endpoint /recipe/{recipy_idx} return http status
        code 200"""

        async with AsyncClient(
            transport=ASGITransport(app=app), base_url="http://test"
        ) as ac:
            response = await ac.get("/recipe/1")
            assert response.status_code == 200

    @pytest.mark.asyncio
    async def test_recipe_details_status_code_400(self) -> None:
        """Test that endpoint /recipe/{recipy_idx} return http status
        code 400"""

        async with AsyncClient(
            transport=ASGITransport(app=app), base_url="http://test"
        ) as ac:
            response = await ac.get("/recipe/0")
            assert response.status_code == 400

    @pytest.mark.asyncio
    async def test_recipe_details_status_code_422(self) -> None:
        """Test that endpoint /recipe/{recipy_idx} return http status
        code 422"""

        async with AsyncClient(
            transport=ASGITransport(app=app), base_url="http://test"
        ) as ac:
            response = await ac.get("/recipe/one")
            assert response.status_code == 422

    @pytest.mark.asyncio
    async def test_recipe_details_success_response_message(self) -> None:
        """Test that endpoint /recipe/{recipy_idx} return correct
        success response data"""

        async with AsyncClient(
            transport=ASGITransport(app=app), base_url="http://test"
        ) as ac:
            response = await ac.get("/recipe/1")
            assert response.json() == correct_recipe_1_message

    @pytest.mark.asyncio
    async def test_recipe_details_error_response_message(self) -> None:
        """Test that endpoint /recipe/{recipy_idx} return correct
        error message for status code 400"""

        async with AsyncClient(
            transport=ASGITransport(app=app), base_url="http://test"
        ) as ac:
            response = await ac.get("/recipe/0")
            assert response.json() == correct_recipe_0_error_message
